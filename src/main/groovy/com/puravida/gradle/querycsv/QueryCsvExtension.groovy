package com.puravida.gradle.querycsv
import org.gradle.api.Project
import org.gradle.api.provider.Property
class QueryCsvExtension {

    QueryCsvExtension(Project project) {
        database = project.objects.property(String)
        database.set("build/csv")
    }

    final Property<String>  database

    void database(Object f){
        this.database.set(f)
    }


}
