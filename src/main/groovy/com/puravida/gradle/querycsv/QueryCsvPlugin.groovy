package com.puravida.gradle.querycsv

import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.DependencySet

class QueryCsvPlugin implements Plugin<Project> {

    void apply(Project project) {
        project.with {
            apply(plugin: 'base')

            QueryCsvExtension csvExtension = extensions.create('csv', QueryCsvExtension, project)

            task('queryCsv', type:QueryCSVTask, group:'csv',description: 'query a csv imported'){
                database = csvExtension.database
            }

        }
    }
}
