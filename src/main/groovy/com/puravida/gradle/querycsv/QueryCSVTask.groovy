package com.puravida.gradle.querycsv

import groovy.sql.Sql
import org.apache.derby.jdbc.EmbeddedDataSource
import org.gradle.api.DefaultTask
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction

class QueryCSVTask extends DefaultTask{

    private Object csvObject
    private String queryString

    @InputFile
    @Optional
    File getCsv(){
        if( csvObject )
            project.file(csvObject)
        else
            null
    }

    void setCsv(Object csvFile){
        this.csvObject = csvFile
    }

    void csv(Object csvFile){
        this.csvObject = csvFile
    }

    @Optional
    @Input
    String getQuery(){
        queryString
    }

    void setQuery(String queryString){
        this.queryString = queryString
    }

    void csv(String queryString){
        this.queryString = queryString
    }

    @Optional
    List<Map> queryResult

    @Optional
    @Input
    String tableName

    @Optional
    @Input
    String separator = ';'


    @Internal
    Property<String> database = project.objects.property(String)

    @TaskAction
    void main(){

        connect()

        if( csv && csv.exists() )
            runImportCSV()

        if( queryString)
            executeQuery(queryString)

        disconnect()
    }

    @Internal
    EmbeddedDataSource embeddedDataSource

    void connect(){
        String bbdd = database.get()
        embeddedDataSource = new EmbeddedDataSource(databaseName: "build/$bbdd", createDatabase: "create")
        tableName = tableName ?: csv.name.split('\\.').dropRight(1).join('')
        logger.info "queryCsv into table $tableName"
    }

    void disconnect(){
        embeddedDataSource.connection.close()
    }

    void runImportCSV(){
        Sql sql = new Sql(embeddedDataSource)

        List<String>header
        csv.withReader{
            header = it.readLine().split(separator) as List<String>

            try{
                sql.execute('delete from '+tableName)
            }catch(Exception e){
                logger.info "$tableName not exist, creating it"
                List<String> createTableScript = ["CREATE TABLE $tableName ("]
                header.eachWithIndex{ f, i->
                    f = f.replaceAll('"','')
                    createTableScript.add "$f varchar(250) ${i==header.size()-1?'':','}"
                }
                createTableScript.add ")"
                String createTable = createTableScript.join(' ')
                sql.execute createTable
            }
        }

        boolean first = true
        List<String> qry = ["INSERT INTO $tableName ("]
        header.eachWithIndex{ f, i->
            f = f.replaceAll('"','')
            qry.add "$f ${i==header.size()-1?'':','}"
        }
        qry.add ") VALUES ("
        header.eachWithIndex{ f, i->
            qry.add "${i==header.size()-1?'?':'?,'}"
        }
        qry.add ")"
        sql.withBatch(1000, qry.join(' ')) { ps ->
            csv.eachLine { line ->
                if (first) {
                    first = false
                    return
                }
                List<String> fields = line.split(separator) as List<String>
                if( fields.size() == header.size())
                    ps.addBatch fields
            }
        }
    }

    void executeQuery(String query){
        try {
            Sql sql = new Sql(embeddedDataSource)
            queryResult = sql.rows(query)
        }catch(e){
            logger.error e.message
        }
    }

}
