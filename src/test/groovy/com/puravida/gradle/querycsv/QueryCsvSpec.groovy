package com.puravida.gradle.querycsv
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
class QueryCsvSpec extends Specification{

    @Rule
    TemporaryFolder testProjectDir

    File buildFile

    def setup() {
        buildFile = testProjectDir.newFile('build.gradle')
        buildFile << """
// tag::addPlugin[]

            plugins {
                id 'com.puravida.gradle.query-csv'
            }
                        
// end::addPlugin[]
            """
    }

    def "run 1 csv task"() {
        File dir = testProjectDir.newFolder("csv")
        File csvFile = new File('test1.csv', dir)
        csvFile.text = """a;b;c
1;2;3
4;5;6
"""

        buildFile << """

// tag::executeTask[]
            csv{
                database 'test' 
            }
            queryCsv{          
                csv = file('csv/test1.csv')      
            }
// end::executeTask[]
            
        """

        when:

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                'queryCsv',
                '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        then:
        result.task(":queryCsv").outcome == SUCCESS
    }

    def "run huge remote csv task"() {
        buildFile << """

// tag::executeHugeTask[]
            csv{
                database 'test' 
            }
            task prepare(){
                doLast{
                    file('biblios.csv').text =
                        'https://datos.madrid.es/egob/catalogo/207075-2-bibliotecas-prestamos.csv'.toURL().getText("iso-8859-1")
                }
            }
            queryCsv{          
                csv = file('biblios.csv')
                query "select Biblioteca from biblios "
                doLast{
                    queryResult.each{println it}
                }      
            }
            queryCsv.dependsOn prepare
// end::executeHugeTask[]
            
        """

        when:

        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments(
                'queryCsv',
                '--stacktrace')
                .withPluginClasspath()
                .withDebug(true)
                .build()

        println result.output
        then:
        result.task(":queryCsv").outcome == SUCCESS
    }

}
