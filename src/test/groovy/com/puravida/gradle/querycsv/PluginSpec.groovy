package com.puravida.gradle.querycsv


import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class PluginSpec  extends Specification{

    Project project

    def setup() {
        project = ProjectBuilder.builder().build()
    }

    @SuppressWarnings('MethodName')
    def "Applies plugin and checks default setup"() {

        expect:
        project.tasks.findByName("queryCsv") == null

        when:
        project.apply plugin: 'com.puravida.gradle.query-csv'

        then:
        Task task = project.tasks.findByName('queryCsv')
        task != null

        project.tasks.findByName('clean') != null
    }

}
